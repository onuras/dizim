#!/usr/bin/env python

from distutils.core import setup

setup(name='diziim',
      version='0.1.0',
      description='dizi bot',
      author='Onur Aslan',
      author_email='onur@users.noreply.github.com',
      license='MIT',
      url='',
      packages=['diziim']
      )


import datetime
from peewee import *  # NOQA

db = SqliteDatabase('dizim.db')


class Dizi(Model):

    title = CharField()
    slug = CharField(unique=True)
    description = TextField()
    poster = CharField()
    last_update = DateTimeField(default=datetime.datetime.now)
    episode_count = IntegerField(default=0)
    imdb_score = IntegerField(default=0)
    source_url = CharField()
    source = CharField()

    class Meta:
        database = db


class Episode(Model):

    dizi = ForeignKeyField(Dizi)
    title = CharField()
    # This is stupid title turkish sites using
    # i.e: X 1. Sezon 2. Bolum,
    #      X 1. Sezon BELGESEL Bolum (LOL)
    fake_title = CharField()
    watch_url = CharField()
    sources = TextField()

    class Meta:
        database = db

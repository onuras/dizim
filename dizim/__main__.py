#!/usr/bin/env python3

import argparse
import logging
import sys
from bot_dizimag import Dizimag
from db import db, Dizi, Episode  # NOQA


def create_database():
    logging.info("Creating database")
    db.connect()
    db.create_tables([Dizi, Episode], safe=True)


def dizimag_init():
    d = Dizimag()
    d.get_all_dizi_details_and_save()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-d', '--debug',
        help="print lots of debugging statements",
        action="store_const", dest="loglevel", const=logging.DEBUG,
        default=logging.WARNING,
    )
    parser.add_argument(
        '-v', '--verbose',
        help="be verbose",
        action="store_const", dest="loglevel", const=logging.INFO,
    )
    parser.add_argument('--create-database', action='store_true',
                        help='create database and tables')
    parser.add_argument('--dizimag-init', action='store_true',
                        help='Initialize dizimag')

    args = parser.parse_args()

    logging.basicConfig(level=args.loglevel)
    # I don't want all info messages from requests
    logging.getLogger("requests").setLevel(logging.WARNING)

    if len(sys.argv) == 1:
        parser.print_help()
    if args.create_database:
        create_database()
    if args.dizimag_init:
        dizimag_init()


from __future__ import unicode_literals

import re
import requests
import json
import logging
from bs4 import BeautifulSoup
from unidecode import unidecode


class Bot:

    """ Generic Bot class """

    def __init__(self, domain, https=False):
        """
        Constructor

        :domain: domain name bot will use
        :https: use https connection (default: False)
        """
        self._domain = domain
        self._https = https
        self._host = ('https' if https else 'http') + '://' + domain
        self._encoding = "utf-8"
        self._r = None
        self._soup = None

    def requests(self):
        """ Returns requests object for extreme cases """
        # FIXME: wtf is extreme case mean
        return requests

    def load_json(self):
        """ Loads json from content and returns json object """
        return json.loads(self._r.content.decode())

    def get(self, url):
        """
        GETs url from domain

        :url: Relative or absolute url
        :returns: Status code of request
        """
        url = self._host + '/' + url if not re.match('^http.?//', url) else url
        logging.info("Getting URL: {}".format(url))
        self._r = requests.get(url)
        self._r.encoding = self._encoding
        return self._r.status_code

    def soup(self):
        """ Makes a BeautifulSoup object from content and returns it """
        self._soup = BeautifulSoup(self._r.text, 'html.parser')
        return self._soup

    def search(self, regex):
        """ Searches regex in content """
        return re.search(regex, self._r.text)

    def generate_slug(self, title):
        """ Generates slug from title """
        title = unidecode(title.lower())
        # Remove non-word characters from begining and end
        title = re.sub('^\W+', '', title)
        title = re.sub('\W+$', '', title)
        # Replace remaining non-word characters with -
        title = re.sub('\W+', '-', title)
        # Replace replications
        title = re.sub('-+', '-', title)
        # One more lower required for chinese
        title = unidecode(title.lower())
        return title

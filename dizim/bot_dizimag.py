
from __future__ import unicode_literals

import re
import logging
import json
from bot import Bot
from db import db, Dizi, Episode


class Dizimag(Bot):

    """
    Dizimag.co bot

    Why dizimag? Dizimag is providing tv shows without hard subs
    """

    def __init__(self):
        Bot.__init__(self, 'dizimag.co')

    def get_dizi_listing_page(self):
        return self.get('yabanci-diziler.html')

    def get_dizi_urls(self):
        """
        Gets all dizi urls from dizimag

        :returns: List of dizi urls
        """
        logging.info("Getting dizi list")
        self.get_dizi_listing_page()
        page = self.soup()
        table = page.find(id='tablelist')
        url_list = list()
        for link in table.find_all('a'):
            if (not re.match('^http://dizimag.co', link.get('href')) or
                    link.get('class') is None or
                    ''.join(link.get('class')) != 's2'):
                continue
            url_list.append(link.get('href'))

        return url_list

    def parse_episode(self, episode_row):
        """
        Parse episode from episode_row

        :episode_row: bs4 object of episode table row
        :returns: Dictionary of episode fake_title, watch_url and title
        """
        # Best way to parse this row is checking 'a' indexes
        # First a is avatar
        # Second title: 1. Sezon 1. Bolum etc. and stored in fake_title
        # Third: Original title
        # All hrefs are leading to watch page
        for i, link in enumerate(episode_row.find_all('a')):
            if (i == 1):
                fake_title = link.get_text()
                watch_url = link.get('href')
            elif (i == 2):
                title = link.get_text()

        logging.debug("Episode: {} parsed".format(title))

        return {
            'fake_title': fake_title,
            'watch_url': watch_url,
            'title': title
        }

    def get_dizi_details(self, url):
        """
        Gets dizi details

        :url: Dizi home page url
        :returns: Dictionary of dizi details
        """
        self.get(url)
        page = self.soup()

        title = page.find('span', {'class': 'gdizi'}).get_text()
        description = page.find('span', itemprop='description').get_text()
        # Dizimag is hosting nice posters
        poster = page.find('div', {'class': 'anares'}) \
                     .find('img', {'class': 'fp'}).get('src')

        # Parsing episodes
        episodes = list()

        for episode_row in page.find_all('tr', {'bgcolor': '444444'}):
            episodes.append(self.parse_episode(episode_row))

        logging.debug("Dizi {} parsed".format(title))
        return {
            'title': title,
            'description': description,
            'poster': poster,
            'episodes': episodes
        }

    def get_episode_id(self, watch_url):
        """ Get episode service id from watch page """
        logging.debug("Getting episode id from: {}".format(watch_url))
        self.get(watch_url)

        # noobcake developer is keeping id
        # inside javascript I have to use re
        return int(self.search('data:\'id=(\d+)\'').group(1))

    def get_episode_sources(self, episode_id):
        """
        Gets episode video source links from episode id

        :episode_id:
        :returns: List of video sources
        """
        logging.info("Getting episode sources for id: {}".format(episode_id))

        # Dizimag is using /service/part to get source urls
        # Example: curl 'http://dizimag.co/service/part' \
        #               -H 'X-Requested-With: XMLHttpRequest' --data 'id=29275'
        # FIXME: This is really an extreme case scenario and
        #        I should never use requests manually in this module
        self._r = self.requests() \
                      .post('http://' + self._domain + '/service/part',
                            headers={'X-Requested-With': 'XMLHttpRequest'},
                            data={'id': episode_id})

        # dizimag is returning "hata" if there is an error
        # return empty list in this case
        if self._r.text == '"hata"':
            logging.error("Unable to get episode sources for id: {}"
                          .format(episode_id))
            return list()

        # example data structure of noobcake php developer
        # guy never heard arrays I guess
        #   {'videokalite4': 720, 'tag': 'dmgposSS', 'videolink4': 'https://',
        #    'videokalite1': 360, 'videolink1': 'https://', 'alt': 'altyazi2'}
        # tag and alt never used in dizimag, they are junk
        json_resp = self.load_json()

        video_sources = list()

        for key, val in json_resp.items():
            match = re.search('^videokalite(\d+)', key)
            if match:
                index = match.group(1)
                video_sources.append({
                    'url': json_resp['videolink' + index],
                    'quality': json_resp['videokalite' + index]
                })

        return video_sources

    def get_episode_detail(self, episode):
        """
        Gets episode details of 'episode' type returned from get_dizi_details

        :episodes: Basic episode data type parsed by parse_episode
        :returns: Detailed episode information
        """
        episode_id = self.get_episode_id(episode['watch_url'])
        episode['sources'] = self.get_episode_sources(episode_id)
        return episode

    def get_all_episode_details_and_save(self, dizi, episodes):
        """
        This is similar to get_episode_detail, but works with list and
        requires dizi database object. It's also saving episodes to database.

        :dizi: dizi database object
        :episodes: Basic list of episodes parsed by parse_episode
        :returns: List of detailed episode information
        """
        for episode_details in episodes:
            self.get_episode_detail(episode_details)
            episode = Episode(dizi=dizi,
                              title=episode_details['title'],
                              fake_title=episode_details['fake_title'],
                              watch_url=episode_details['watch_url'],
                              sources=json.dumps(
                                  episode_details['sources']))
            episode.save()

        return episodes

    def get_all_dizi_details_and_save(self):
        db.connect()
        dizi_urls = self.get_dizi_urls()

        logging.info("Getting every dizi details from dizimag")

        for url in dizi_urls:
            dizi_details = self.get_dizi_details(url)
            dizi = Dizi(title=dizi_details['title'],
                        slug=self.generate_slug(dizi_details['title']),
                        description=dizi_details['description'],
                        poster=dizi_details['poster'],
                        source=self._domain,
                        source_url=url)
            dizi.save()

            self.get_all_episode_details_and_save(dizi,
                                                  dizi_details['episodes'])

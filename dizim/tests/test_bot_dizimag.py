
import unittest
from bot_dizimag import Dizimag


class TestBotDizimag(unittest.TestCase):

    def test_get_dizi_listing_page(self):
        d = Dizimag()
        r = d.get_dizi_listing_page()
        self.assertEqual(r, 200)

    def test_dizimag_soup(self):
        d = Dizimag()
        d.get_dizi_listing_page()
        soup = d.soup()
        self.assertIsNotNone(soup.title)
        self.assertRegexpMatches(soup.title.get_text(), 'Dizimag')

    def test_get_dizi_urls(self):
        d = Dizimag()
        urls = d.get_dizi_urls()
        self.assertGreater(len(urls), 0)

        for url in urls:
            self.assertRegexpMatches(url, '^http://dizimag.co')

    def test_get_dizi_details(self):
        d = Dizimag()
        details = d.get_dizi_details('tahtin-oyunlari')

        self.assertEqual(details['title'], 'Game of Thrones')
        self.assertRegexpMatches(details['poster'], '^http://dizimag.co.*jpg$')
        self.assertGreater(len(details['description']), 0)
        self.assertGreater(len(details['episodes']), 0)

        # test episodes
        for episode in details['episodes']:
            self.assertGreater(len(episode['fake_title']), 0)
            self.assertGreater(len(episode['title']), 0)
            self.assertRegexpMatches(episode['watch_url'],
                                     '.*izle-dizi.html$')

    def test_get_episode_id(self):
        d = Dizimag()
        # try to get episode id for 'GOT S05E10'
        url = 'tahtin-oyunlari/5-sezon-1-bolum-izle-dizi.html'
        episode_id = d.get_episode_id(url)
        self.assertIsInstance(episode_id, int)
        self.assertEqual(episode_id, 29315)

    def test_get_episode_sources(self):
        d = Dizimag()
        sources = d.get_episode_sources(29315)
        self.assertGreater(len(sources), 0)
        for source in sources:
            self.assertIn('url', source)
            self.assertRegexpMatches(source['url'], '^http.?://')
            self.assertIn('quality', source)
            self.assertIsInstance(source['quality'], int)

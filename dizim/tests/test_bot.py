
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
import unittest
from bot import Bot

# domain used in tests
DOMAIN = 'google.com'


class TestBot(unittest.TestCase):

    def test_get(self):
        b = Bot(DOMAIN)
        b.get('')
        self.assertGreaterEqual(len(b._r.content), 0)

    def test_soup(self):
        b = Bot(DOMAIN)
        b.get('')
        soup = b.soup()
        self.assertGreaterEqual(len(soup.title), 0)

    def test_search(self):
        b = Bot(DOMAIN)
        b.get('')
        # FIXME: I assume there is some int in google's home page
        #        This test is not reliable
        some_int = b.search('(\d+)')
        self.assertIsInstance(int(some_int.group(1)), int)

    def test_generate_slug(self):
        b = Bot(DOMAIN)
        slug = b.generate_slug("         ---ONURŞŞğğaSlan @#$zzzz  -@-  ")
        self.assertEqual(slug, 'onurssggaslan-zzzz')
        # Who knows I may fetch chinese titles
        self.assertEqual(b.generate_slug(u"\u5317\u4EB0"), 'bei-jing')

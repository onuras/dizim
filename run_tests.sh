#!/bin/sh

export PYTHONPATH=./dizim

python -m flake8 dizim
python3 -m flake8 dizim

echo "Running tests with nosetests"
nosetests dizim -v "$@"

echo "Running tests with nose2-3"
nose2-3 dizim -v "$@"
